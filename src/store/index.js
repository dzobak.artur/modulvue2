import { createStore } from "vuex";
import products from "./modules/products";

export default createStore({
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    products,
  },
});
