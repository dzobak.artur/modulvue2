import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import NavBar from "../components/NavBar.vue";
import DishList from "../views/DishList.vue"
import DishForm from "../views/DishForm"
const routes = [

  {
    path: "/",
    name: "main",
    component: NavBar,
    children: [
      
      
    ]
    
  },
  {
    path: "/home",
    name: "home",
    component: HomeView,
  },
  {
    path: "/dishList",
    name: "dishList",
    component: DishList   
  },
  {
    path: "/dishForm",
    name: "dishForm",
    component: DishForm   
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
  
  
  
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
